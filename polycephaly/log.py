#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

import logbook

logger_group = logbook.LoggerGroup()
logger_group.level = logbook.NOTICE

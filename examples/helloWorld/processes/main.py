#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Polycephaly
import polycephaly.core
import polycephaly.functions.dynamic

# Formatting
from pprint import pformat as pf

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Process( polycephaly.core.Process ):

	# This is a callback method that replies back to a message.
	@polycephaly.functions.dynamic.methodDecorator(
		subject		=	r'(?i)^SALUTATION$',
	)
	def salutation( self, message ):

		logger.debug( f"'{ self.name }' received a salutation message from '{ message[ 'sender' ] }':\n{ pf( message ) }" )

		# Respond to a received message.
		reply	=	self.reply(

						# Message parameters
						message,
						body	=	"Hi, thanks for writing.",

						# Extra message headers
						acme	=	123,

					)
		logger.debug( f"'{ self.name }' sent a reply to '{ reply[ 'recipient' ] }':\n{ pf( reply ) }" )

		pass # END CALLBACK : Salutation

	pass # END CLASS : PROCESS : Main

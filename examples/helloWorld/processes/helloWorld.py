#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Polycephaly
import polycephaly.core

# Formatting
from pprint import pformat as pf

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Process( polycephaly.core.Process ):

	def life( self ):

		# Send a message to the Main process.
		message		=	self.send(

							# Message parameters
							recipient	=	self.nameMain,
							subject		=	'salutation',
							body		=	'Hello, World!',

							# Extra message headers
							args		=	self.args,
							kwargs		=	self.kwargs,

						)
		logger.debug( f"'{ self.name }' sent a message to '{ message[ 'recipient' ] }':\n{ pf( message ) }" )

		# Wait for reply from the Main process.
		reply		=	self.waitForReply( message, timeout=10 )
		logger.debug( f"'{ self.name }' received a reply from '{ reply.get( 'sender' ) }':\n{ pf( reply ) }" )

		pass # END METHOD : Life

	pass # END CLASS : PROCESS : Hello, World!

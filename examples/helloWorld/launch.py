#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# System
import sys
sys.dont_write_bytecode = True

# Polycephaly
import polycephaly.core

# Application
import processes

#---------------------------------------------------- LOGGING RELATED PARAMETERS

# Global
import logbook
import logbook.more

# Custom handler to include optional color.
class StreamHandler(
	logbook.more.ColorizingStreamHandlerMixin,
	logbook.StreamHandler,
):
	pass # END CLASS

# Handlers
stream_handler	=	StreamHandler(
						sys.stdout,
					)
error_handler	=	logbook.StderrHandler()
null_handler	=	logbook.NullHandler()

# Push stream handler to application
stream_handler.push_application()

# Logging - copy and paste this block into every process that you create.
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

# Application
logger.level							=	logbook.INFO
polycephaly.logger_group.level			=	logbook.INFO

# Processes
processes.main.logger.level				=	logbook.DEBUG
processes.helloWorld.logger.level		=	logbook.DEBUG

#----------------------------------------------------/LOGGING RELATED PARAMETERS

class Application( polycephaly.core.Application ):

	def build( self ):

		# Update global frequency
		self.globalFrequency( 15 )				# Run fifteen times per second.

		# Add process : Hello, World!
		self.addProcess(

			processes.helloWorld,				# If the default class name of `Process` is used, it doesn't need to be specified here.

			# Arguments to pass through to the process.
			'Arg1',
			'Arg2',

			# Keyword arguments to pass through to the process.
			abc				=	123,
			xyz				=	789,

			# Process Parameters
			name			=	'Hello',		# Override the default name of `helloWorld` with a shorter name of `hello`.
			mode			=	'Thread',		# Run the process as a thread.
			frequency		=	1 / 5,			# Update the local frequency to run once every 5 seconds.
			autostart		=	True,			# This is default behavior, with the alternative being to setup a process, and then start it at a later time.
			boundShutdown	=	False,			# Run independently without binding to main process.

		)

		pass # END METHOD : Build

	pass # END CLASS : Application

if __name__ == '__main__':

	logger.notice( "Start : 'Hello, World!'." )

	Application(

		# Add process : Main
		processes.main,
		name			=	'Main',			# Set the name of the process that we refer to, or specify an added process as main.
		ppill			=	'STOP!',		# Case-sensitive poison pill.
		queueSize		=	25,				# Maximum number of messages to keep in each queue.
		queueType		=	'FIFO',			# FIFO or Priority message queue.
		frequency		=	5,				# Update the local frequency to run five times per second.
		forceStop		=	True,			# Allow the process to ignore repeated shutdown requests.
		threadsTimeout	=	30,				# Application will wait on threads for this long.

	).run()

	logger.notice( "Stop : 'Hello, World!'." )

	pass # END MAIN

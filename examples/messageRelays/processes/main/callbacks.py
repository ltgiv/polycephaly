#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Polycephaly
import polycephaly.functions.dynamic

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Extend( object ):

	@polycephaly.functions.dynamic.methodDecorator(
		subject		=	'reply'
	)
	def reply( self, message ):

		self.sendNextMessage	=	True

		if ( message.get( 'body' ) == True ) ^ ( message.get( 'body' ) == False ):
			logger.notice( f"'{ self.name }' received '{ message[ 'body' ] }' from '{ message[ 'sender' ] }'" )
			pass # END IF

		pass # END

	pass # END CLASS : Main : Callbacks

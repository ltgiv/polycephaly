#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Polycephaly
import polycephaly.core

# Formatting
from pprint import pformat as pf

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Process( polycephaly.core.Process ):

	# Call-back : fall through messages which didn't match any other filter.
	@polycephaly.functions.dynamic.methodDecorator()
	def fallthrough( self, message ):
		logger.debug( f"'{ self.name }' received a message that fell through from '{ message[ 'sender' ] }':\n{ pf( message ) }" )
		pass # END CALLBACK : Fall-through

	# Call-back : failure when trying to receive a message.
	@polycephaly.functions.dynamic.methodDecorator(
		failed		=	True,
	)
	def failthrough( self, message ):
		logger.debug( f"'{ self.name }' failed to receive a message:\n{ pf( message ) }" )
		pass # END CALLBACK : Fail-through

	pass # END CLASS : PROCESS : Main

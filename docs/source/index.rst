.. Polycephaly documentation master file, created by
   sphinx-quickstart on Sun Oct 27 13:05:46 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Polycephaly's documentation!
=======================================

Easily create system daemons (and programs) that use an email-like syntax for communicating between threaded and forked processes, to avoid something along the lines of this:

.. image:: _static/multithreaded_programming_puppies.jpg

.. automodule:: polycephaly
    :members:

References
==================

* `Introduction <https://gitlab.com/ltgiv/polycephaly/blob/master/README.md>`_

* `Source <https://gitlab.com/ltgiv/polycephaly>`_

* `Tracker <https://gitlab.com/ltgiv/polycephaly/issues>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Application <polycephaly/polycephaly.core.application>
   Process <polycephaly/polycephaly.core.process>
   Communications <polycephaly/polycephaly.comms>
   Functions <polycephaly/polycephaly.functions>
   Examples <examples/index.rst>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# System
import time

# Reflection / Debugging
import inspect

# Math
import random

# Polycephaly
import polycephaly.core
import polycephaly.functions.dynamic

# Formatting
from pprint import pformat as pf

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Process( polycephaly.core.Process ):

	def _threadSkel( self, prefix=None, number=None ):
		'''
		This method is the basis of dynamically built _thread_{ 0-2 }() methods.
		'''

		lifeTime	=	random.randint( 15, 20 )
		for instance in range( lifeTime ):

			logger.info( f"Round { instance + 1 } of { lifeTime } from { self.name }.{ prefix }{ number }()" )

			if not self.lightSleeper( sleep=1, callback=self.isActive ):
				return

			pass # END FOR

		pass # END METHOD : Thread Skeleton

	def _thread_infinite( self ):

		# Frame info.
		curframe	=	inspect.currentframe()
		calframe	=	inspect.getouterframes( curframe, 2 )

		i	=	0
		while self.isActive():

			logger.info( f"Round { i + 1 } from { self.name }.{ calframe[ 0 ][ 3 ] }()" )

			i	+=	1
			time.sleep( 1 )

			pass # END WHILE LOOP

		logger.notice( f"The '{ self.name }' process is no longer active, and { self.name }.{ calframe[ 0 ][ 3 ] }() has finished its task." )

		pass # END Thread : Infinite

	def birth( self ):

		# Build self._thread_0(), self._thread_1(), and self._thread_2() based upon self._threadSkel()
		for i in range( 3 ):

			logger.debug( f"Building dynamic method : { self.name }._thread_{ i }" )

			polycephaly.functions.dynamic.add_dynamo(
				skel	=	self._threadSkel,
				prefix	=	'_thread_',
				number	=	i,
				cls		=	self,
			)

			pass # END FOR

		'''
		launchThreads() will launch any other method starting with `_thread_`.
		You can tie your `_thread_` methods back to this process' lifetime by using `while self.isActive()` if you need them to be persistent and receptive with the rest of Polycephaly.
		Another option is self.lightSleeper() which allows you to monitor for changes and use similar to time.sleep() but combined with self.isActive()

		With Threads 0-2, I demonstrate the use of lightSleeper(), while as with Thread "Infinite", I demonstrate the use of isActive() for an endless loop that will shutdown alongside
		of the main process once Threads 0-2 have completed.
		'''
		self.launchThreads()

		pass # END METHOD : Birth

	def life( self ):

		logger.info( f"Current thread table:\n{ pf( self.getChildThreads() ) }" )

		# When a child thread dies, it's removed from the table by this method.
		self.childThreadJanitor()

		if not self.listChildThreadsLives( True, *range( 3 ) ):
			logger.notice( f"Child threads 0-2 of '{ self.name }' have finished, now exiting the application." )
			self.ebrake( 'Nothing left for us to do.' )
			pass # END IF

		pass # END METHOD : Life

	pass # END CLASS : PROCESS : Main

#!/usr/bin/env bash
: <<'!COMMENT'

Polycephaly Sphinx Generator
Louis T. Getterman IV
Thad.Getterman.org

!COMMENT

################################################################################
SOURCE="${BASH_SOURCE[0]}" # Dave Dopson, Thank You! - http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$SCRIPTPATH/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
################################################################################
SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SCRIPTNAME=`basename "$SOURCE"`
POLYCEPHALYPATH="$( cd "$(dirname "${SCRIPTPATH}/../../")" ; pwd -P )"
################################################################################

# Use this for starting out from scratch.
sphinx-apidoc \
	-o "${POLYCEPHALYPATH}/docs/source/polycephaly/" \
	"${POLYCEPHALYPATH}/polycephaly/" \
	;

# Examples
sphinx-apidoc \
	-o "${POLYCEPHALYPATH}/docs/source/examples/" \
	"${POLYCEPHALYPATH}/examples" \
	;

# Date and Time
sphinx-apidoc \
	-o "${POLYCEPHALYPATH}/docs/source/examples/dateAndTime/" \
	"${POLYCEPHALYPATH}/examples/dateAndTime" \
	;

# Fall or Fail
sphinx-apidoc \
	-o "${POLYCEPHALYPATH}/docs/source/examples/fallOrFail/" \
	"${POLYCEPHALYPATH}/examples/fallOrFail" \
	;

# Hello, World!
sphinx-apidoc \
	-o "${POLYCEPHALYPATH}/docs/source/examples/helloWorld/" \
	"${POLYCEPHALYPATH}/examples/helloWorld" \
	;

# Locking
sphinx-apidoc \
	-o "${POLYCEPHALYPATH}/docs/source/examples/locking/" \
	"${POLYCEPHALYPATH}/examples/locking" \
	;

# Skeleton
sphinx-apidoc \
	-o "${POLYCEPHALYPATH}/docs/source/examples/skel/" \
	"${POLYCEPHALYPATH}/examples/skel" \
	;

# Thread Spinner
sphinx-apidoc \
	-o "${POLYCEPHALYPATH}/docs/source/examples/threadSpinner/" \
	"${POLYCEPHALYPATH}/examples/threadSpinner" \
	;

eval python "${POLYCEPHALYPATH}/setup.py" build_sphinx

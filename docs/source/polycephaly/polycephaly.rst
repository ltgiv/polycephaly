polycephaly package
===================

Subpackages
-----------

.. toctree::

   polycephaly.comms
   polycephaly.core
   polycephaly.functions
   polycephaly.processes

Submodules
----------

polycephaly.log module
----------------------

.. automodule:: polycephaly.log
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: polycephaly
   :members:
   :undoc-members:
   :show-inheritance:

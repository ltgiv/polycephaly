#!/usr/bin/env bash
: <<'!COMMENT'

Polycephaly Sphinx Generator
Louis T. Getterman IV
Thad.Getterman.org

!COMMENT

################################################################################
SOURCE="${BASH_SOURCE[0]}" # Dave Dopson, Thank You! - http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$SCRIPTPATH/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
################################################################################
SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SCRIPTNAME=`basename "$SOURCE"`
POLYCEPHALYPATH="$( cd "$(dirname "${SCRIPTPATH}/../../")" ; pwd -P )"
################################################################################

eval pdoc \
	--html \
	--html-dir="${POLYCEPHALYPATH}/docs/pdoc/" \
	--overwrite \
	"${POLYCEPHALYPATH}/polycephaly/" \
	;

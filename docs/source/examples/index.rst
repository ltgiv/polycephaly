.. Polycephaly documentation master file, created by
   sphinx-quickstart on Sun Oct 27 13:05:46 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Examples
=======================================

.. automodule:: polycephaly
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Date and Time <dateAndTime/launch>
   Fall or Fail <fallOrFail/launch>
   Hello, World! <helloWorld/launch>
   Locking <locking/launch>
   Skeleton <skel/launch>
   Thread Spinner <threadSpinner/launch>

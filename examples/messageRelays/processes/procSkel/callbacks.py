#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Math
import random

# Types
import string

# Polycephaly
import polycephaly.functions.dynamic

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Extend( object ):

	@polycephaly.functions.dynamic.methodDecorator(
		subject		=	r'(?i)^TAG!$',
	)
	def tag( self, message ):

		randLetters	=	''.join( random.sample( string.ascii_letters, 3 ) )
		expecting	=	randLetters[ ::-1 ]

		# Tell '{ randLetters }', expect '{ expecting }'
		logger.notice( f"'{ self.name }' : '{ self.nameMain }' told me to tag '{ message[ 'prey' ] }'." )

		message2	=	self.send(
						recipient	=	message[ 'prey' ],
						subject		=	"PING",
						body		=	randLetters,
					)

		reply	=	self.waitForReply( message2, timeout=15 )
		del message2

		if reply.get( 'body' ) == expecting:
			logger.notice( f"'{ self.name }' : received CORRECT answer from '{ message[ 'prey' ] }'." )
			self.reply( message, body=True )
			pass # END IF

		else:
			logger.warning( f"'{ self.name }' : incorrect answer from '{ message[ 'prey' ] }' (expecting '{ expecting }', but received '{ reply.get( 'body' ) }')." )
			self.reply( message, body=False )
			pass # END ELSE

		pass # END METHOD

	@polycephaly.functions.dynamic.methodDecorator(
		subject		=	r'(?i)^PING$',
	)
	def ping( self, message ):

		self.reply(
			message,
			body	=	message[ 'body' ][ ::-1 ],
		)

		pass # END METHOD

	pass # END CLASS : Main : Callbacks

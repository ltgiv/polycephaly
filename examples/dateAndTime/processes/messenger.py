#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Polycephaly
import polycephaly.core

# Formatting
from pprint import pformat as pf

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Process( polycephaly.core.Process ):

	def life( self ):

		for request in [
			'Date',
			'Time',
			'DateTime',
			'UTC',
		]:

			message	=	self.send(
							recipient	=	self.nameMain,
							subject		=	request,
						)
			reply	=	self.waitForReply( message )
			logger.debug( f"'{ self.name }' queried { message[ 'subject' ] } and received '{ reply.get( 'body' ) }'." )

			pass # END FOR : Request

		pass # END METHOD : Life

	pass # END CLASS : PROCESS : Main

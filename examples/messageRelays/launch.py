#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# System
import sys
sys.dont_write_bytecode = True

# Polycephaly
import polycephaly.core

# Application
import processes

#---------------------------------------------------- LOGGING RELATED PARAMETERS

# Global
import logbook
import logbook.more

# Custom handler to include optional color.
class StreamHandler(
	logbook.more.ColorizingStreamHandlerMixin,
	logbook.StreamHandler,
):
	pass # END CLASS

# Handlers
stream_handler	=	StreamHandler(
						sys.stdout,
					)
error_handler	=	logbook.StderrHandler()
null_handler	=	logbook.NullHandler()

# Push stream handler to application
stream_handler.push_application()

# Logging - copy and paste this block into every process that you create.
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

# Application
logger.level												=	logbook.INFO
polycephaly.logger_group.level								=	logbook.INFO

#----------------------------------------------------/LOGGING RELATED PARAMETERS

class Application( polycephaly.core.Application ):

	def build( self ):

		self.globalFrequency( 30 )

		# Process 1
		self.addProcess(
			processes.procSkel,
			mode	=	'Threading',
			name	=	'proc1',
		)

		# Process 2
		self.addProcess(
			processes.procSkel,
			mode	=	'Threading',
			name	=	'proc2',
		)

		# Process 3
		self.addProcess(
			processes.procSkel,
			mode	=	'Multiprocessing',
			name	=	'proc3',
		)

		# Process 4
		self.addProcess(
			processes.procSkel,
			mode	=	'Multiprocessing',
			name	=	'proc4',
		)

		pass # END METHOD : Build

	pass # END CLASS : Application

if __name__ == '__main__':

	logger.notice( "Start : 'Message relays'." )

	Application( processes.main ).run()

	logger.notice( "Stop : 'Message relays'." )

	pass # END MAIN

#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Math
import random

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Extend( object ):

	# Reset by reply() callback once a random sub-process responds.
	sendNextMessage	=	True

	def life( self ):

		predator, prey	=	random.sample(
								[ sp for sp in self.listRecipients() if sp != self.nameMain ],
								2
							)

		# Tell a random process to tag another random process
		if self.sendNextMessage:

			message	=	self.send(
							subject		=	"TAG!",
							recipient	=	predator,
							prey		=	prey,
						)

			self.sendNextMessage	=	False

			pass # END IF

		# Check for new messages, and run appropriate callbacks.
		self.mailman()

		pass # END METHOD : Life

	pass # END CLASS : Main : Events

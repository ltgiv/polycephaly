#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# System
import sys
sys.dont_write_bytecode = True

# Polycephaly
import polycephaly.core

# Application
import processes

#---------------------------------------------------- LOGGING RELATED PARAMETERS

# Global
import logbook
import logbook.more

# Custom handler to include optional color.
class StreamHandler(
	logbook.more.ColorizingStreamHandlerMixin,
	logbook.StreamHandler,
):
	pass # END CLASS

# Handlers
stream_handler	=	StreamHandler(
						sys.stdout,
					)
error_handler	=	logbook.StderrHandler()
null_handler	=	logbook.NullHandler()

# Push stream handler to application
stream_handler.push_application()

# Logging - copy and paste this block into every process that you create.
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

# Application
logger.level												=	logbook.DEBUG
polycephaly.logger_group.level								=	logbook.DEBUG

# Silence extraneous messages that occur for various background activities.
polycephaly.comms.messenger.actions.logger.level			=	logbook.INFO
polycephaly.core.process.setup.logger.level					=	logbook.NOTICE

# Levels : Processes
# processes.main.logger.level									=	logbook.INFO

#----------------------------------------------------/LOGGING RELATED PARAMETERS

class Application( polycephaly.core.Application ):

	def build( self ):

		self.globalFrequency( 1 )

		pass # END METHOD : Build

	pass # END CLASS : Application

if __name__ == '__main__':

	logger.notice( "Start : 'Thread Spinner'." )

	Application( processes.main ).run()

	logger.notice( "Stop : 'Thread Spinner'." )

	pass # END MAIN

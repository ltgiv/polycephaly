polycephaly.core package
========================

Subpackages
-----------

.. toctree::

   polycephaly.core.application
   polycephaly.core.process

Module contents
---------------

.. automodule:: polycephaly.core
   :members:
   :undoc-members:
   :show-inheritance:

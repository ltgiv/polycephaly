#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Polycephaly
import polycephaly.core

# Formatting
from pprint import pformat as pf

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Process( polycephaly.core.Process ):

	def life( self ):

		message	=	self.send(

						# Message parameters
						recipient	=	self.nameMain,
						subject		=	'Testing',
						body		=	'Hi, this is a test message.',

						# Extra message headers
						args		=	self.args,
						kwargs		=	self.kwargs,

					)
		logger.debug( f"'{ self.name }' sent a message to '{ message[ 'recipient' ] }':\n{ pf( message ) }" )

		# Check for new messages, and run appropriate callbacks.
		self.mailman()

		pass # END METHOD : Life

	pass # END CLASS : PROCESS : Main

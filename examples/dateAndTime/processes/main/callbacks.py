#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Time
import datetime

# Formatting
from pprint import pformat as pf

# Polycephaly
import polycephaly.functions.dynamic

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Extend( object ):

	@polycephaly.functions.dynamic.methodDecorator(
		subject		=	r'(?i)^DATE$',
	)
	def _cbDate( self, message ):

		# Respond to a received message.
		reply	=	self.reply(

						# Message parameters
						message,
						body	=	datetime.datetime.now( datetime.timezone.utc ).strftime("%Y-%m-%d"),

					)
		logger.debug( f"'{ self.name }' sent a reply to '{ reply[ 'recipient' ] }':\n{ pf( reply ) }" )

		pass # END CALLBACK : Date

	@polycephaly.functions.dynamic.methodDecorator(
		subject		=	r'(?i)^TIME$',
	)
	def _cbTime( self, message ):

		# Respond to a received message.
		reply	=	self.reply(

						# Message parameters
						message,
						body	=	datetime.datetime.now( datetime.timezone.utc ).strftime("%H:%M:%S"),

					)
		logger.debug( f"'{ self.name }' sent a reply to '{ reply[ 'recipient' ] }':\n{ pf( reply ) }" )

		pass # END CALLBACK : Time

	@polycephaly.functions.dynamic.methodDecorator(
		subject		=	r'(?i)^DATETIME$',
	)
	def _cbDateTime( self, message ):

		# Respond to a received message.
		reply	=	self.reply(

						# Message parameters
						message,
						body	=	datetime.datetime.now( datetime.timezone.utc ).strftime("%Y-%m-%d %H:%M:%S"),

					)
		logger.debug( f"'{ self.name }' sent a reply to '{ reply[ 'recipient' ] }':\n{ pf( reply ) }" )

		pass # END CALLBACK : Date and Time

	@polycephaly.functions.dynamic.methodDecorator(
		subject		=	r'(?i)^UTC$',
	)
	def _cbUTC( self, message ):

		# Respond to a received message.
		reply	=	self.reply(

						# Message parameters
						message,
						body	=	datetime.datetime.now( datetime.timezone.utc ).timestamp(),

					)
		logger.debug( f"'{ self.name }' sent a reply to '{ reply[ 'recipient' ] }':\n{ pf( reply ) }" )

		pass # END CALLBACK : UTC

	pass # END CLASS : Main : Callbacks

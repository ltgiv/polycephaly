#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# System
import os

# Math
import locale
locale.setlocale(locale.LC_ALL, '')

# Polycephaly
import polycephaly.core
import polycephaly.functions.dynamic

# Pattern matching
import glob

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Process( polycephaly.core.Process ):

	sharedTotal	=	0

	def _threadSkel( self, prefix=None, number=None, contribute=0 ):

		contribution	=	0
		while self.isActive():

			# We've met our quota, let's stop.
			if contribution == contribute:
				logger.info( f"{ self.name }.{ prefix }{ number }() has finished contributing {contribute:n} to the shared total." )
				return

			# Add 1 onto the total using the context manager.
			with self.getLocks( 'sharedInteger' ):
				self.sharedTotal += 1
				pass # END WITH : Shared integer lock

			# Add 1 onto the total using the locks methods.
			self.getLocks( 'sharedInteger' ).acquire()
			self.sharedTotal += 1
			self.getLocks( 'sharedInteger' ).release()

			contribution += 2

			pass # END WHILE LOOP

		pass # END METHOD : Thread Skeleton

	def birth( self ):

		logger.notice( 'Demonstration of locks for child threads relentlessly adding on to the same variable.' )

		self.addLocks( 'sharedInteger' )

		# Create 50 threads that will each contribute 2,000 to the pot, for a total of 100,000.
		for i in range( 50 ):

			logger.debug( f"Building dynamic method : { self.name }._thread_{ i }" )

			polycephaly.functions.dynamic.add_dynamo(
				skel		=	self._threadSkel,
				prefix		=	'_thread_',
				number		=	i,
				cls			=	self,
				contribute	=	2000,
			)

			pass # END FOR

		self.launchThreads()

		pass # END METHOD : Birth

	def life( self ):

		with self.getLocks( 'sharedInteger' ):
			logger.notice( f"Sum is now : {self.sharedTotal:n}" )
			pass # END WITH : Shared integer lock

		# When a child thread dies, it's removed from the table by this method.
		self.childThreadJanitor()

		if not self.listChildThreadsLives( True ):
			self.ebrake( f"All child threads have now finished their contributions." )
			pass # END IF

		pass # END METHOD : Life

	def death( self ):

		logger.notice( f'Locking demonstration has completed with a total of {self.sharedTotal:n}.' )

		pass # END METHOD : Death

	pass # END CLASS : Process

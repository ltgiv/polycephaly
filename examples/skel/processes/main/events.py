#!/usr/bin/env python -u
# -*- coding: utf-8 -*-

# Logging
from logbook import Logger
from polycephaly.log import logger_group
logger = Logger( __loader__.name )
logger_group.add_logger( logger )

class Extend( object ):

	def birth( self ):
		pass # END METHOD : Birth

	def life( self ):

		# Check for new messages, and run appropriate callbacks.
		self.mailman()

		pass # END METHOD : Life

	def death( self ):
		pass # END METHOD : Death

	pass # END CLASS : Main : Events
